package com.zenika.forum;


import com.zenika.forum.domain.model.answer.BadLengthContentAnswerException;
import com.zenika.forum.domain.model.question.BadLengthTitleQuestionException;
import com.zenika.forum.domain.model.question.NoQuestionMarkQuestionException;
import com.zenika.forum.domain.model.question.QuestionNotFoundException;
import com.zenika.forum.domain.model.user.UserNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ExceptionsHandler {

    @ExceptionHandler(value = BadLengthTitleQuestionException.class)
    public final ResponseEntity<ApiError> badLengthTitleException(BadLengthTitleQuestionException e) {
        return new ResponseEntity<>(new ApiError(HttpStatus.BAD_REQUEST, "Le titre ne doit pas dépasser 20 mots."), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = NoQuestionMarkQuestionException.class)
    public final ResponseEntity<ApiError> noQuestionMarkQuestionException(NoQuestionMarkQuestionException e) {
        return new ResponseEntity<>(new ApiError(HttpStatus.BAD_REQUEST, "Le titre doit être une question se terminant par un '?'."), HttpStatus.BAD_REQUEST);
    }


    @ExceptionHandler(value = BadLengthContentAnswerException.class)
    public final ResponseEntity <ApiError> badLengthContentAnswerException(BadLengthContentAnswerException e) {
        return new ResponseEntity<>(new ApiError(HttpStatus.BAD_REQUEST, "Le contenu de votre réponse ne doit pas dépasser 100 mots."), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = QuestionNotFoundException.class)
    public final ResponseEntity <ApiError> questionNotFoundException(QuestionNotFoundException e) {
        return new ResponseEntity<>(new ApiError(HttpStatus.NOT_FOUND, "Cette question n'existe pas"), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = UserNotFoundException.class)
    public final ResponseEntity <ApiError> userNotFoundException(UserNotFoundException e) {
        return new ResponseEntity<>(new ApiError(HttpStatus.NOT_FOUND, "Cet utilisateur n'existe pas"), HttpStatus.NOT_FOUND);
    }
}