package com.zenika.forum.web.users;

import com.zenika.forum.application.UserService;
import com.zenika.forum.domain.model.user.User;
import com.zenika.forum.domain.model.user.UserNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@RequestMapping("/api/users")
public class UserController {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    //lister les users
    @GetMapping()
    public List<CreatedUserDto> listUsers(@RequestParam(name="username", required = false)String username) {
        Iterable<User> allUsers = Collections.emptyList();
        if(username != null) {
            allUsers = this.userService.searchUserByUsername(username);
        } else {
            allUsers = this.userService.listUsers();
        }

        List<CreatedUserDto> users = new ArrayList<>();
        for(User user : allUsers) {
            users.add(new CreatedUserDto(user.getId(), user.getEmail(), user.getUsername()));
        }
        return users;
    }

    //get user par son id
    @GetMapping("/id/{id}")
    public List <CreatedUserDto> findUserById(@PathVariable String id) throws UserNotFoundException {
       List < Optional<User>> userById = Collections.singletonList(this.userService.getUserBId(id));
       List <CreatedUserDto> userDto = new ArrayList<>();
       try {
           Optional.of(userById).orElseThrow(UserNotFoundException::new).forEach(user -> {
               userDto.add(new CreatedUserDto(user.get().getId(), user.get().getEmail(), user.get().getUsername()));
           });
           return userDto;
       }
       catch (NoSuchElementException e) {
           throw new UserNotFoundException();
       }
    }


    //vérifier si le user existe en base
    @PostMapping("/me")
    @ResponseStatus(value = HttpStatus.OK)
    public String me() {
        return SecurityContextHolder.getContext().getAuthentication().getName();
    }

    //créer un user
    @PostMapping()
    @ResponseStatus(value = HttpStatus.CREATED)
    public CreatedUserDto createUser(@RequestBody CreateUserDto createUserDto) {
        User user = this.userService.createUser(createUserDto.getEmail(), createUserDto.getUsername(), createUserDto.getPassword());
        return new CreatedUserDto(user.getId(), user.getEmail(), user.getUsername());
    }
}
