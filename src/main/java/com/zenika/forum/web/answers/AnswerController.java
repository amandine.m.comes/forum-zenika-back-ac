package com.zenika.forum.web.answers;

import com.zenika.forum.application.AnswerService;
import com.zenika.forum.domain.model.answer.Answer;
import com.zenika.forum.domain.model.answer.BadLengthContentAnswerException;
import com.zenika.forum.domain.model.question.QuestionNotFoundException;
import com.zenika.forum.domain.model.user.UserNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/answers")
public class AnswerController {

    private final AnswerService answerService;


    public AnswerController(AnswerService answerService) {
        this.answerService = answerService;
    }

    //Répondre à une question
    @PostMapping("/{questionId}")
    @ResponseStatus(value = HttpStatus.CREATED)
    public Answer createAnswer(@PathVariable String questionId, @RequestBody CreateAnswerDto createAnswerDto) throws BadLengthContentAnswerException, QuestionNotFoundException, UserNotFoundException {
        return this.answerService.createAnswer(createAnswerDto.getContent(), questionId);
    }

    //Afficher les réponses à une question
    @GetMapping("/question/{questionId}")
    @ResponseStatus(value = HttpStatus.OK)
    List<Answer> findAnswersToQuestion(@PathVariable String questionId) throws QuestionNotFoundException {
        return this.answerService.findQuestionAnswers(questionId);
    }

    //Get Answers by UserId
    @GetMapping("/user/{userid}")
    @ResponseStatus(value = HttpStatus.OK)
    List <Answer> findAnswerByUserId (@PathVariable String userid)  {
        return answerService.findAnswersByUserId(userid);
    }

}
