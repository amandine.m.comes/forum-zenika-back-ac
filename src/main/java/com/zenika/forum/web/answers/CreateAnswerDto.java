package com.zenika.forum.web.answers;

public class CreateAnswerDto {

    private String content;

    protected CreateAnswerDto() {
        // // for deserialisation
    }

    public CreateAnswerDto(String content) {
        this.content = content;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
