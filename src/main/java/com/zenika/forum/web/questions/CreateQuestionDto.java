package com.zenika.forum.web.questions;


public class CreateQuestionDto {

    private String title;
    private String content;

    public CreateQuestionDto(String title, String content){
        this.title = title;
        this.content = content;
    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
