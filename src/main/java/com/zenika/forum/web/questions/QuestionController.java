package com.zenika.forum.web.questions;

import com.zenika.forum.application.QuestionService;
import com.zenika.forum.domain.model.question.BadLengthTitleQuestionException;
import com.zenika.forum.domain.model.question.NoQuestionMarkQuestionException;
import com.zenika.forum.domain.model.question.Question;
import com.zenika.forum.domain.model.question.QuestionNotFoundException;
import com.zenika.forum.domain.model.user.UserNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@RestController
@RequestMapping("/api/questions")
public class QuestionController {

    private QuestionService questionService;

    @Autowired
    public QuestionController(QuestionService questionService) {
        this.questionService = questionService;
    }

    //Créer une question
    @PostMapping()
    @ResponseStatus(value = HttpStatus.CREATED)
    public Question createQuestion(@RequestBody CreateQuestionDto createQuestionDto) throws BadLengthTitleQuestionException, UserNotFoundException, NoQuestionMarkQuestionException {
        return this.questionService.createQuestion(createQuestionDto.getTitle(), createQuestionDto.getContent());
    }

    //Lister toutes les questions en mappant dans un DTO
    @GetMapping()
    @ResponseStatus(value = HttpStatus.OK)
    public List <CreatedQuestionDto> fetchQuestions(@RequestParam(name="title", required = false) String title){
       List <Question> allQuestions = Collections.emptyList();
               if(title != null){
                   allQuestions = this.questionService.getAllQuestionsByTitle(title);
               } else {
                   allQuestions = this.questionService.getAllQuestions();
               }

        List<CreatedQuestionDto> questionsDto = new ArrayList<>();
        for(Question question : allQuestions) {
            questionsDto.add(new CreatedQuestionDto(question.getId(), question.getDate(), question.getTitle(), question.getContent()));
        }
        return questionsDto;
    }

    //Get Question By Id
    @GetMapping("/id/{id}")
    ResponseEntity <Question> findQuestionById (@PathVariable String id) throws QuestionNotFoundException {
        return questionService.getQuestionById(id).map(ResponseEntity::ok).orElseThrow(QuestionNotFoundException::new);
    }

    //Get Question By UserId
    @GetMapping("/user/{userid}")
    @ResponseStatus(value = HttpStatus.OK)
   List <Question> findQuestionByUserId (@PathVariable String userid)  {
        return questionService.findQuestionsByUserId(userid);
    }

}
