package com.zenika.forum.web.questions;

import java.sql.Date;
import java.time.OffsetDateTime;

public class CreatedQuestionDto {

    private String id;
    private OffsetDateTime date;
    private String title;
    private String content;

    public CreatedQuestionDto(String id, OffsetDateTime date, String title, String content) {
        this.id = id;
        this.date = date;
        this.title = title;
        this.content = content;
    }

    public String getId() {
        return id;
    }

    public OffsetDateTime getDate() {
        return date;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
