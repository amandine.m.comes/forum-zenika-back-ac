package com.zenika.forum.configuration;

import com.zenika.forum.domain.repository.UserRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class SecurityConfiguration {

    //Gestion de la sécurité : requêtes autorisées aux visiteurs et requêtes accessibles qu'aux utilisateurs
    @Bean
    SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        return http
                .antMatcher("/**")
                .csrf().disable() // CSRF protection is enabled by default in the Java configuration.
                .authorizeRequests(authorize -> authorize
                        //get questions
                        .antMatchers(HttpMethod.GET, "/api/questions").permitAll()
                        .antMatchers(HttpMethod.GET, "/api/questions/id/*").permitAll()
                        .antMatchers(HttpMethod.GET, "/api/questions/user/*").permitAll()
                        //get answers
                        .antMatchers(HttpMethod.GET, "/api/answers/question/*").permitAll()
                        .antMatchers(HttpMethod.GET, "/api/answers/user/*").permitAll()
                        //get users
                        .antMatchers(HttpMethod.GET, "/api/users").permitAll()
                        .antMatchers(HttpMethod.GET, "/api/users/id/*").permitAll()
                        //create user
                        .antMatchers(HttpMethod.POST, "/api/users").permitAll()
                        .anyRequest().authenticated()
                )
                // Activate httpBasic Authentication https://en.wikipedia.org/wiki/Basic_access_authentication
                .httpBasic()
                .and()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .cors()
                .and()
                .build();
    }

    //Gestion des erreurs de cors
    @Bean
    public WebMvcConfigurer corsConfigurer() {
        return new WebMvcConfigurer() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("/**").allowedOrigins("http://localhost:4200", "http://localhost").allowedMethods("HEAD",
                        "GET", "POST", "PUT", "DELETE", "PATCH");
            }
        };
    }

    //implémente UserDetailsService : pour gérer l'authentification et la création des users
    @Bean
    UserDetailsService userDetailsService(UserRepository userRepository) {
        return new UserDetailsService() {
            @Override
            public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
                return userRepository.findByUsername(username).orElse(null);
            }
        };
    }

    //encoder le password à l'inscription et à l'authentification
    @Bean
    public PasswordEncoder encoder() {
        return new BCryptPasswordEncoder();
    }


}
