package com.zenika.forum.domain.repository;

import com.zenika.forum.domain.model.answer.Answer;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface AnswerRepository extends CrudRepository <Answer, String> {

    List<Answer> findByQuestionIdOrderByAnswerDateDesc(String questionId);

    List <Answer> findByUserId(String userId);
}
