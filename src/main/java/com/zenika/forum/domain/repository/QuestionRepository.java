package com.zenika.forum.domain.repository;

import com.zenika.forum.domain.model.question.Question;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface QuestionRepository extends CrudRepository<Question, String> {

    List<Question> findByOrderByQuestionDateDesc();

    @Query(value = "SELECT *\n" +
            "FROM questions\n" +
            "WHERE word_similarity(?1, questions.title) > 0.2",
         nativeQuery = true)
    List <Question> findByTitle(String title);

    List <Question> findByUserId(String userid);

}
