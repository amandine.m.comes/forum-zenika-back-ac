package com.zenika.forum.domain.repository;

import com.zenika.forum.domain.model.user.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


import java.util.Optional;

@Repository
public interface UserRepository extends CrudRepository <User, String> {

    Optional <User> findByUsername(String username);

    @Query(value = "SELECT *\n" +
            "FROM users\n" +
            "WHERE word_similarity(?1, users.username) > 0.2", nativeQuery = true)
    Iterable <User> getUserByUsername(String username);
}
