package com.zenika.forum.domain.model.answer;

import com.zenika.forum.domain.model.question.Question;
import com.zenika.forum.domain.model.user.User;

import javax.persistence.*;
import java.sql.Date;
import java.time.OffsetDateTime;
import java.util.Optional;

@Entity
@Table(name="answers")
@Access(AccessType.FIELD)
public class Answer {

    @Id
    private String id;
    private OffsetDateTime answerDate;
    private String content;
    @ManyToOne
    @JoinColumn(name = "question_id")
    private Question question;
    @ManyToOne
    @JoinColumn(name = "answer_user_id")
    private User user;



    protected Answer() {
      //FOR JPA
  }

  public Answer(String id, OffsetDateTime answerDate, String content, Question question, User user) {
      this.id = id;
      this.answerDate = answerDate;
      this.content = content;
      this.question = question;
      this.user = user;
  }


    public String getId() {
        return id;
    }

    public OffsetDateTime getAnswerDate() {
        return answerDate;
    }

    public String getContent() {
        return content;
    }

    public Question getQuestion() {
        return question;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

}

