package com.zenika.forum.domain.model.question;

import com.zenika.forum.domain.model.user.User;

import javax.persistence.*;
import java.sql.Date;
import java.time.OffsetDateTime;

@Entity
@Table(name="questions")
@Access(AccessType.FIELD)
public class Question {

    @Id
    private String id;
    private OffsetDateTime questionDate;
    private String title;
    private String content;
    @ManyToOne
    @JoinColumn(name = "question_user_id")
    private User user;


    protected Question(){
        //FOR JPA
    }

    public Question(String id, OffsetDateTime questionDate, String title, String content, User user ) {
        this.id = id;
        this.questionDate = questionDate;
        this.title = title;
        this.content = content;
        this.user = user;
    }

    public String getId() {
        return this.id;
    }

    public OffsetDateTime getDate() {
        return this.questionDate;
    }

    public String getTitle() {
        return this.title;
    }

    public String getContent() {
        return this.content;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
