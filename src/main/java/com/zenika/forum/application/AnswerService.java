package com.zenika.forum.application;

import com.zenika.forum.domain.model.answer.Answer;
import com.zenika.forum.domain.model.answer.BadLengthContentAnswerException;
import com.zenika.forum.domain.model.question.Question;
import com.zenika.forum.domain.model.question.QuestionNotFoundException;
import com.zenika.forum.domain.model.user.User;
import com.zenika.forum.domain.model.user.UserNotFoundException;
import com.zenika.forum.domain.repository.AnswerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.time.OffsetDateTime;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class AnswerService {

    private final AnswerRepository answerRepository;
    private final QuestionService questionService;
    private final UserService userService;

    @Autowired
    public AnswerService(AnswerRepository answerRepository, QuestionService questionService, UserService userService) {
        this.answerRepository = answerRepository;
        this.questionService = questionService;
        this.userService = userService;
    }

    //Créer une réponse
    public Answer createAnswer(String content, String questionId) throws BadLengthContentAnswerException, QuestionNotFoundException, UserNotFoundException {
        String[] words = content.split(" ");
        int numberOfWords = words.length;
        if (numberOfWords <= 100) {
            //Retrouver la question par son Id
            Optional<Question> questionById = questionService.getQuestionById(questionId);
            Question question = questionById.orElseThrow(QuestionNotFoundException::new);
            //Injecter la date de la réponse : timestamp
            OffsetDateTime offsetDateTime = OffsetDateTime.now().withNano(0);
            //Retrouver le user par son username
            Optional <User> userByUsername = userService.getUserByUsername(SecurityContextHolder.getContext().getAuthentication().getName());
            User user = userByUsername.orElseThrow(UserNotFoundException::new);
            //Créer la réponse
            Answer answer = new Answer(UUID.randomUUID().toString(), offsetDateTime, content, question, user);
            answer = answerRepository.save(answer);
            return answer;
        } else {
            throw new BadLengthContentAnswerException();
        }
    }

    //Lister les réponses d'une question
    public List<Answer> findQuestionAnswers(String questionId) {
        return answerRepository.findByQuestionIdOrderByAnswerDateDesc(questionId);
        }

    //Chercher les réponses d'un user
    public List <Answer> findAnswersByUserId(String userId) {
        return answerRepository.findByUserId(userId);
    }
}
