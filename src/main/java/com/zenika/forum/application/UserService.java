package com.zenika.forum.application;

import com.zenika.forum.domain.model.user.User;
import com.zenika.forum.domain.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class UserService {

    private final UserRepository userRepository;
    private PasswordEncoder passwordEncoder;


    @Autowired
    public UserService(UserRepository userRepository, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    //Créer un user
    public User createUser(String username, String email, String password) {
        User newUser = new User(UUID.randomUUID().toString(), username, email, password);
        newUser.setPassword(passwordEncoder.encode(password));
        userRepository.save(newUser);
        return newUser;
    }

    //Retrouver un user par son username : utilisée par security configuration
    public Optional<User> getUserByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    //Retrouver un user par son Id
    public Optional<User> getUserBId(String id) {
        return userRepository.findById(id);
    }

    //Retourner la liste de tous les users
    public Iterable<User> listUsers() {
        return userRepository.findAll();
    }

    //Retourner la liste d'un user par son username
    public Iterable<User> searchUserByUsername(String username) {
        return this.userRepository.getUserByUsername(username);
    }

}