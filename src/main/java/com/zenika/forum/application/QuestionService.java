package com.zenika.forum.application;


import com.zenika.forum.domain.model.question.BadLengthTitleQuestionException;
import com.zenika.forum.domain.model.question.NoQuestionMarkQuestionException;
import com.zenika.forum.domain.model.question.Question;
import com.zenika.forum.domain.model.user.User;
import com.zenika.forum.domain.model.user.UserNotFoundException;
import com.zenika.forum.domain.repository.QuestionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.time.OffsetDateTime;
import java.util.UUID;

@Service
public class QuestionService {

    private final QuestionRepository questionRepository;
    private final UserService userService;

    @Autowired
    public QuestionService(QuestionRepository questionRepository, UserService userService) {
        this.questionRepository = questionRepository;
        this.userService = userService;
    }

    //Créer une question
    public Question createQuestion(String title, String content) throws BadLengthTitleQuestionException, UserNotFoundException, NoQuestionMarkQuestionException {
        String[] words = title.split(" ");
        int numberOfWords = words.length;
        if (numberOfWords > 20) {
            throw new BadLengthTitleQuestionException();
        } else if(!words[numberOfWords -1].contains("?")) {
            throw new NoQuestionMarkQuestionException();
        } else {
            //Injecter la date de la question : timestamp
            OffsetDateTime offsetDateTime = OffsetDateTime.now().withNano(0);
            //Retrouver le user par son username
            Optional <User> userByUsername = userService.getUserByUsername(SecurityContextHolder.getContext().getAuthentication().getName());
            User user = userByUsername.orElseThrow(UserNotFoundException::new);
            //Créer la question
            Question question = new Question(UUID.randomUUID().toString(), offsetDateTime, title, content, user);
            question = questionRepository.save(question);
            return question;
        }
        /*
        if (numberOfWords <= 20 && words[numberOfWords -1].contains("?")) {
            //Injecter la date de la question : timestamp
           OffsetDateTime offsetDateTime = OffsetDateTime.now().withNano(0);
            //Retrouver le user par son username
            Optional <User> userByUsername = userService.getUserByUsername(SecurityContextHolder.getContext().getAuthentication().getName());
            User user = userByUsername.orElseThrow(UserNotFoundException::new);
           //Créer la question
            Question question = new Question(UUID.randomUUID().toString(), offsetDateTime, title, content, user);
            question = questionRepository.save(question);
            return question;
        } else {
            throw new BadTitleQuestionException();
        }

         */
    }

    //Lister les questions
    public List<Question> getAllQuestions(){return this.questionRepository.findByOrderByQuestionDateDesc();}

    //Retrouver une question par son ID
    public Optional<Question> getQuestionById(String id) {return this.questionRepository.findById(id);}

  //Chercher une question par son titre
    public List <Question> getAllQuestionsByTitle(String title){
        return questionRepository.findByTitle(title);
    }

    //Chercher les questions d'un user
    public List <Question> findQuestionsByUserId(String userId) {
        return questionRepository.findByUserId(userId);
    }
}
