CREATE EXTENSION IF NOT EXISTS pg_trgm;

CREATE TABLE IF NOT EXISTS users
(
   id         char(36) primary key,
   email       text not null unique,
   username    text not null unique,
   password    text not null
);

CREATE TABLE IF NOT EXISTS questions
(
    id                  char(36) primary key,
    question_date       TIMESTAMP not null,
    title               text not null,
    content             text not null,
    question_user_id    char(36) REFERENCES users(id)
);

CREATE TABLE IF NOT EXISTS answers
(
    id                char(36) primary key,
    answer_date       TIMESTAMP not null,
    content           text not null,
    question_id       char(36) REFERENCES questions(id),
    answer_user_id    char(36) REFERENCES users(id)
)


