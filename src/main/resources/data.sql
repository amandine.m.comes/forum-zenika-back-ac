-- insérer des users
INSERT INTO users(id, email, username, password)
VALUES('de10ec32-862c-4bb6-8f12-0eaf68ab96e4', 'docker@mail.com', 'docker', '$2a$10$Iaqu0s248gmCILuy5FP6HudZ031eeFDes.jVWgQ3CaGanOmO.6VN2') on conflict do nothing;

INSERT INTO users(id, email, username, password)
VALUES ('5c1f237f-6be5-441e-b74a-5aab92344841', 'amandine@mail.com', 'amandine', '$2a$10$Fz50enplFNd16K9sKQa7SeIQn96TzT9LCQpC2pqo5GSeb27yG8YAK') on conflict do nothing;


-- insérer une question
INSERT INTO questions(id, question_date, title, content, question_user_id)
VALUES ('ac43bf3b-bb04-468c-83f3-29e94e8bb913', '2022-05-05 10:36:22', 'Comment créer un docker-compose?',
'Je souhaite créer un docker-compose avec une image front et une image back, comment faire?', 'de10ec32-862c-4bb6-8f12-0eaf68ab96e4') on conflict do nothing;

-- insérer une réponse
INSERT INTO answers(id, answer_date, content, question_id, answer_user_id)
VALUES ('f161850e-f6cb-4c6c-b5a3-a1b649a9ce80', '2022-05-05 12:36:22', 'Bonjour Docker, tu peux suivre cette doc : ...', 'ac43bf3b-bb04-468c-83f3-29e94e8bb913', '5c1f237f-6be5-441e-b74a-5aab92344841') on conflict do nothing;



