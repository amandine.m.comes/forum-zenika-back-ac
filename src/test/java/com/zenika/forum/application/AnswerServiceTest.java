package com.zenika.forum.application;

import com.zenika.forum.domain.model.answer.Answer;
import com.zenika.forum.domain.model.answer.BadLengthContentAnswerException;
import com.zenika.forum.domain.model.question.Question;
import com.zenika.forum.domain.model.question.QuestionNotFoundException;
import com.zenika.forum.domain.model.user.User;
import com.zenika.forum.domain.model.user.UserNotFoundException;
import com.zenika.forum.domain.repository.AnswerRepository;
import com.zenika.forum.domain.repository.QuestionRepository;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;

import java.time.OffsetDateTime;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;

@SpringBootTest
@ActiveProfiles("test")
public class AnswerServiceTest {

        @MockBean
        UserService userService;

        @MockBean
        QuestionRepository questionRepository;

        @Autowired
        QuestionService questionService;

        @MockBean
        AnswerRepository answerRepository;

        @Autowired
        AnswerService answerService;

        @Test
        @WithMockUser(username = "amandine")
        void testCreateAnswerOk() throws UserNotFoundException, QuestionNotFoundException, BadLengthContentAnswerException {
            //Mock UserService
            Mockito.when(userService.getUserByUsername("amandine")).thenReturn(Optional.of(new User("123", "amandine@mail.com", "amandine", "password")));

            Optional <User> userByUsername = userService.getUserByUsername(SecurityContextHolder.getContext().getAuthentication().getName());
            User user = userByUsername.orElseThrow(UserNotFoundException::new);

            //Get date
            OffsetDateTime offsetDateTime = OffsetDateTime.now().withNano(0);

            //Mock QuestionRepository
            Question question = new Question("456", offsetDateTime, "test?", "content", user);
            Mockito.when(questionRepository.save(any(Question.class))).thenReturn(question);
            Mockito.when(questionRepository.findById("456")).thenReturn(Optional.of(question));
            Question questionById = questionService.getQuestionById("456").orElseThrow(QuestionNotFoundException::new);

            //Mock AnswerRepository
            Answer answer = new Answer("789", offsetDateTime, "content", questionById, user);
            Mockito.when(answerRepository.save(any(Answer.class))).thenReturn(answer);

            //Test Create Answer
            Answer createdAnswer = answerService.createAnswer(answer.getContent(), answer.getQuestion().getId());
            assertEquals(createdAnswer.getId(), answer.getId());
        }

        @Test
        @WithMockUser(username = "amandine")
        void testCreateAnswerWithBadLengthContent() throws UserNotFoundException, QuestionNotFoundException, BadLengthContentAnswerException {
            //Mock UserService
            Mockito.when(userService.getUserByUsername("amandine")).thenReturn(Optional.of(new User("123", "amandine@mail.com", "amandine", "password")));

            Optional <User> userByUsername = userService.getUserByUsername(SecurityContextHolder.getContext().getAuthentication().getName());
            User user = userByUsername.orElseThrow(UserNotFoundException::new);

            //Get date
            OffsetDateTime offsetDateTime = OffsetDateTime.now().withNano(0);

            //Mock QuestionRepository
            Question question = new Question("456", offsetDateTime, "test?", "content", user);
            Mockito.when(questionRepository.save(any(Question.class))).thenReturn(question);
            Mockito.when(questionRepository.findById("456")).thenReturn(Optional.of(question));
            Question questionById = questionService.getQuestionById("456").orElseThrow(QuestionNotFoundException::new);

            //Mock AnswerRepository : avec un contenu de 101 mots
            Answer answer = new Answer("789", offsetDateTime, "Lorem ipsum dolor sit amet, consectetur adipiscing elit. " +
                    "Donec mi tellus, vehicula vitae porttitor quis, interdum et ipsum. Morbi eu mauris lorem. Etiam auctor ullamcorper " +
                    "metus in consequat. Vestibulum suscipit mauris ac dolor ultricies porttitor. Suspendisse rutrum nibh ut sapien ornare, " +
                    "et sollicitudin leo scelerisque. Ut vehicula, lectus ut semper commodo, metus ligula ultrices risus, ut posuere quam lacus " +
                    "quis purus. Vestibulum augue dolor, euismod ut orci fermentum, porttitor tempor lacus. Nam libero diam, dictum ut ante quis, " +
                    "elementum molestie libero. Phasellus id laoreet nibh, sed sollicitudin ante. In eget posuere felis. Sed luctus sed odio eget cursus. " +
                    "Suspendisse feugiat purus.", questionById, user);
            Mockito.when(answerRepository.save(any(Answer.class))).thenReturn(answer);

            //Test throw exception
            assertThrows(BadLengthContentAnswerException.class, () -> {
                answerService.createAnswer(answer.getContent(), answer.getQuestion().getId());
            });
        }


}
