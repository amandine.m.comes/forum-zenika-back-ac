package com.zenika.forum.application;

import com.zenika.forum.domain.model.question.BadLengthTitleQuestionException;
import com.zenika.forum.domain.model.question.NoQuestionMarkQuestionException;
import com.zenika.forum.domain.model.question.Question;
import com.zenika.forum.domain.model.user.User;
import com.zenika.forum.domain.model.user.UserNotFoundException;
import com.zenika.forum.domain.repository.QuestionRepository;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;

import java.time.OffsetDateTime;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
@ActiveProfiles("test")
public class QuestionServiceTest {

    @MockBean
    UserService userService;

    @MockBean
    QuestionRepository questionRepository;

    @Autowired
    QuestionService questionService;

    @Test
    @WithMockUser(username = "amandine")
    void testCreateQuestionOk() throws UserNotFoundException, BadLengthTitleQuestionException, NoQuestionMarkQuestionException {
        //Mock UserService
        Mockito.when(userService.getUserByUsername("amandine")).thenReturn(Optional.of(new User("123", "amandine@mail.com", "amandine", "password")));

        Optional <User> userByUsername = userService.getUserByUsername(SecurityContextHolder.getContext().getAuthentication().getName());
        User user = userByUsername.orElseThrow(UserNotFoundException::new);

        //Get date
        OffsetDateTime offsetDateTime = OffsetDateTime.now().withNano(0);

        //Mock QuestionRepository
        Question question = new Question("456", offsetDateTime, "test?", "content", user);
        Mockito.when(questionRepository.save(any(Question.class))).thenReturn(question);

        //Test Create Question
        Question createdQuestion = questionService.createQuestion(question.getTitle(), question.getContent());
        assertEquals(createdQuestion.getId(), question.getId());

    }

    @Test
    @WithMockUser(username = "amandine")
    void testCreateQuestionBadLengthTitle() throws UserNotFoundException {
        //Mock UserService
        Mockito.when(userService.getUserByUsername("amandine")).thenReturn(Optional.of(new User("123", "amandine@mail.com", "amandine", "password")));

        Optional <User> userByUsername = userService.getUserByUsername(SecurityContextHolder.getContext().getAuthentication().getName());
        User user = userByUsername.orElseThrow(UserNotFoundException::new);

        //Get date
        OffsetDateTime offsetDateTime = OffsetDateTime.now().withNano(0);

        //Mock QuestionRepository : Titre de 21 mots
        Question question = new Question("456", offsetDateTime, "Lorem ipsum dolor sit amet, " +
                "consectetur adipiscing elit. Proin vitae nunc consequat, " +
                "venenatis augue at, elementum ligula. Cras nec purus sed?", "content", user);
        Mockito.when(questionRepository.save(any(Question.class))).thenReturn(question);

        //Test throw exception
        assertThrows(BadLengthTitleQuestionException.class, () -> {
            questionService.createQuestion(question.getTitle(), question.getContent());
        });
    }

    @Test
    @WithMockUser(username = "amandine")
    void testCreateQuestionWithNoQuestionMark() throws UserNotFoundException {
        //Mock UserService
        Mockito.when(userService.getUserByUsername("amandine")).thenReturn(Optional.of(new User("123", "amandine@mail.com", "amandine", "password")));

        Optional <User> userByUsername = userService.getUserByUsername(SecurityContextHolder.getContext().getAuthentication().getName());
        User user = userByUsername.orElseThrow(UserNotFoundException::new);

        //Get date
        OffsetDateTime offsetDateTime = OffsetDateTime.now().withNano(0);

        //Mock QuestionRepository : Titre de 20 mots sans "?"
        Question question = new Question("456", offsetDateTime, "Lorem ipsum dolor sit amet, " +
                "consectetur adipiscing elit. Proin vitae nunc consequat, " +
                "venenatis augue at, elementum ligula. Cras nec purus", "content", user);
        Mockito.when(questionRepository.save(any(Question.class))).thenReturn(question);

        //Test throw exception
        assertThrows(NoQuestionMarkQuestionException.class, () -> {
            questionService.createQuestion(question.getTitle(), question.getContent());
        });
    }

}
